<?php 

/**
 * retourn un tableau contenant la liste des fichiers dans le dossier passé en argument
 *
 * @param [type] $path dossier a scanner
 * @return array la liste des noms des fichiers
 */
function scanFolder($path){

    $files = scandir($path);

    unset($files[0]);
    unset($files[1]);

    return $files;
}


function getFolder(){
    return (isset($_GET['folder'])) ? $_GET['folder'] : "/";
}

function getParent(){
    return (getFolder() != "/")?substr(getFolder(), 0, strrpos(getFolder(), "/")) : "/";
}

?>