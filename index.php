<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Index</title>
    <?php include ("./template/bootstrap.php"); ?>
  </head>

  <body>
    <?php include ("./template/navbar.php"); ?>

  <!-- Include particular content -->
    <?php 
        
      if (isset($_GET["page"])) {
          $page = $_GET["page"]; 
      } else {
          $page = "home";
      }
      
      $path = "./content/${page}.php";

      if (is_file($path)) {
          include "./content/${page}.php";
      } else {
          echo "404.<br /> cette page n'existe pas ";
      }
      
    ?>

    <?php include ("./template/footer.php"); ?>

  </body>
</html>