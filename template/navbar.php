<nav class="navbar navbar-dark bg-dark navbar-expand-lg navbar-light bg-light">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">Navbar</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">

        <!-- Elements de la navbar en dynamique -->
        <?php $dir = "./content"; ?>
        <?php
          $navElements = scandir($dir);
          for($i = 2 ; $i < count($navElements) ; $i++):
            $navElement = str_replace(".php", "", $navElements[$i]); ?>
            <li class='nav-item'>
              <a class='nav-link' aria-current='page' href='./?page=<?=$navElement?>'> <?=$navElement?></a>
            </li>
          <?php endfor ?>


        <!-- <li class="nav-item">
          <a class="nav-link" aria-current="page" href="./?page=home">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="./?page=form">Form</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="./?page=messages">Messages</a>
        </li> -->
      </ul>
    </div>
  </div>
</nav>